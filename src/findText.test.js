const findText = require("./findText");
const getFileContent = require("./getFileContent");

jest.mock("./getFileContent");

describe("findText", function () {
  it("Should return true value", async () => {
    getFileContent.mockResolvedValue("Some text response.");

    const filePath = "test.txt";
    const textToFind = "response";

    const result = await findText(filePath, textToFind);

    expect(result).toBe(true);
  });

  it("Should return false value", async () => {
    getFileContent.mockResolvedValue("Some text response.");

    const filePath = "test.txt";
    const textToFind = "house";

    const result = await findText(filePath, textToFind);

    expect(result).toBe(false);
  });

  it("Should return an error", async () => {
    getFileContent.mockRejectedValue(new Error("File not found"));

    const filePath = "blank.txt";
    const textToFind = "sample";

    await expect(findText(filePath, textToFind)).rejects.toThrow(
      "File not found"
    );
  });
});
