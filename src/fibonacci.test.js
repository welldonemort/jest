const getFibonacci = require("./fibonacci");
const { TEST_DATA } = require("./constants");
const { getMessage } = require("./helpers");

const testItemCb = ({ input, expected }) => {
  it(getMessage(expected), () => expect(getFibonacci(input)).toEqual(expected));
};

const fibonacciCb = () => {
  TEST_DATA.forEach(testItemCb);
};

describe("fibonacci", fibonacciCb);
