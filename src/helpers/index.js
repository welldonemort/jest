const getMessage = (expected) => `Should return value ${expected}`;

module.exports = { getMessage };
