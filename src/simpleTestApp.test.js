const startServer = require("./simpleTestApp");
const supertest = require("supertest");

describe("simpleTestApp", function () {
  let server;

  beforeAll(async () => {
    server = await startServer();
  });

  afterAll(() => server.stop());

  it("'/' GET should return an empty array of tasks", async () => {
    const response = await supertest(server).get("/");
    expect(response.status).toBe(200);
    expect(response.body).toEqual([]);
  });

  it("'/' POST should add a task to the list", async () => {
    const newTask = {
      title: "Test task 1",
      description: "Some random description",
    };

    const response = await supertest(server).post("/").send(newTask);

    expect(response.status).toBe(200);

    // check if the task is added
    const getResponse = await supertest(server).get("/");
    expect(getResponse.status).toBe(200);
    expect(getResponse.body.length).toBe(1);
    expect(getResponse.body[0]).toEqual(newTask);
  });

  it("'/' POST should return 400 Bad Request on invalid input", async () => {
    const invalidTask = {
      description: "This is an invalid task",
    };

    const response = await supertest(server).post("/").send(invalidTask);

    expect(response.status).toBe(400);
  });
});
