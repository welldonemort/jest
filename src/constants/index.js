const TEST_DATA = [
  { input: -1, expected: 0 },
  { input: 0, expected: 0 },
  { input: 1, expected: 1 },
  { input: 3, expected: 2 },
  { input: 5, expected: 5 },
  { input: 9, expected: 34 },
];

module.exports = { TEST_DATA };
